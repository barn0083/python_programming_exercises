def game():
    guess_range = random.randit(0, 9)
    correct = True
    while correct:
        print('Guess a number between 0 and 9')
        user_guess1 = int(input())
        guess_try = 0
        guess_limit = 3
        if user_guess1 == guess_range :
            correct = False
            print('Well done. Do you want to play again? (y/n)')
            again = input()
            if again == 'y' :
                continue
            elif again == 'n' :
                print('Goodbye')
                break
        elif user_guess1 > guess_range:
            print('Your guess is too high')
            guess_try += 1
            if guess_try < guess_limit:
                continue
            else:
                print('I won HaHa!')
                print('Do you want to play again? (y/n)')
                again = input()
                if again == 'y':
                    continue
                elif again == 'n':
                    break
        elif user_guess1 < guess_range:
            print('Your guess is too low')
            guess_try += 1
            if guess_try < guess_limit:
                continue
            else:
                print('I won HaHa!')
                print('Do you want to play again? (y/n)')
                again = input()
                if again == 'y':
                    continue
                elif again == 'n':
                    break
        elif user_guess1 >= int(10):
            print('This is out of range')
            guess_try += 1
            if guess_try < guess_limit:
                continue
            else:
                print('I won HaHa!')
                print('Do you want to play again? (y/n)')
                again = input()
                if again == 'y':
                    continue
                elif again == 'n':
                    break
        elif user_guess1 <= int(0):
            print('This is out of range')
            guess_try += 1
            if guess_try < guess_limit:
                continue
            else:
                print('I won HaHa!')
                print('Do you want to play again? (y/n)')
                again = input()
                if again == 'y':
                    continue
                elif again == 'n':
                    break

